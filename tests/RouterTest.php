<?php

namespace SlightlyInteractive\Router\Test;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use SlightlyInteractive\Router\Router;

class RouterTest extends TestCase
{
    /** @var ContainerInterface|MockObject */
    private $container;

    /** @var Router */
    private $router;

    /** @var MockObject[] */
    private $containerObjects;

    public function setUp(): void
    {
        $this->container = $this->createMock(ContainerInterface::class);

        $this->router = new Router();
        $this->router->setDI($this->container);

        $this->middlewares = [];

        $this->container->expects($this->any())
            ->method('get')
            ->will($this->returnCallback(
                function (string $id) {
                    if (isset($this->containerObjects[$id])) {
                        return $this->containerObjects[$id];
                    }

                    throw new class ("{$id} not found") extends \Exception implements NotFoundExceptionInterface {
                    };
                }
            ));

        $this->container->expects($this->any())
            ->method('has')
            ->will($this->returnCallback(
                function (string $id) {
                    return isset($this->containerObjects[$id]);
                }
            ));
    }

    public function testEmptyGroup()
    {
        $handler = $this->createHandler('TestController');
        $handler->expects($this->once())
            ->method('handle')
            ->will($this->returnValue($this->createResponse()));
        $this->router->map('get', '/test', 'TestController')->middlewareGroup('emptyGroup');
        $this->router->dispatch($this->createRequest('get', '/test'));
    }

    public function testEmptyGroupNonRecommended()
    {
        $handler = $this->createHandler('TestController');
        $handler->expects($this->once())
            ->method('handle')
            ->will($this->returnValue($this->createResponse()));
        $this->router->map('get', '/test', 'TestController')->middleware('emptyGroup');
        $this->router->dispatch($this->createRequest('get', '/test'));
    }

    /**
     * @return array[]
     */
    public function providerDispatch()
    {
        $middlewares = [
            'group1' => [
                'Group1Middleware1',
                'Group1Middleware2',
            ],
            'group2' => [
                'Group2Middleware1',
                'Group2Middleware2',
            ],
            'group3' => [
                'Group3Middleware1',
                'Group3Middleware2',
            ],
        ];

        return [
            // no middlewares
            [
                [],
                [
                    [ 'get', '/a/b/c/d/e', 'GetController', [] ],
                    [ 'post', '/a/b/c/d/e', 'PostController', [] ],
                ],
                'get',
                '/a/b/c/d/e',
                [],
                'GetController',
            ],
            // some middlewares
            [
                $middlewares,
                [
                    [ 'get', '/blah', 'BlahController', [ 'group2', 'group3' ] ],
                ],
                'get',
                '/blah',
                [
                    'Group2Middleware1',
                    'Group2Middleware2',
                    'Group3Middleware1',
                    'Group3Middleware2',
                ],
                'BlahController',
            ],
            // a middleware handles things
            [
                $middlewares,
                [
                    [ 'get', '/blah', 'BlahController', [ 'group2', 'group3' ] ],
                ],
                'get',
                '/blah',
                [
                    'Group2Middleware1',
                    'Group2Middleware2',
                    'Group3Middleware1',
                    'Group3Middleware2',
                ],
                'Group3Middleware1',
            ],
            // test parametric maps
            [
                [],
                [
                    [ 'get', '/blah/{param1}/{param2}', 'ParameterController', [] ],
                    [ 'get', '/blah/a/b', 'NonParameterController', [] ],
                ],
                'get',
                '/blah/x/y',
                [],
                'ParameterController',
                [ 'param1' => 'x', 'param2' => 'y' ],
            ],
            // test that the most specific map is followed
            [
                [],
                [
                    [ 'get', '/blah/{param1}/{param2}', 'ParameterController', [] ],
                    [ 'get', '/blah/a/b', 'NonParameterController', [] ],
                ],
                'get',
                '/blah/a/b',
                [],
                'NonParameterController',
            ],
            // test root matches
            [
                [],
                [
                    [ 'get', '/', 'IndexController', [] ],
                ],
                'get',
                '/',
                [],
                'IndexController',
            ],
            // test global middleware
            [
                [
                    '*' => [
                        'GlobalMiddleware1',
                        'GlobalMiddleware2',
                    ]
                ],
                [
                    [ 'get', '/blah', 'BlahController', [] ],
                ],
                'get',
                '/blah',
                [
                    'GlobalMiddleware1',
                    'GlobalMiddleware2',
                ],
                'BlahController',
            ],
            // some issue I was having
            [
                [],
                [
                    [ 'get', '/', 'RootController', [] ],
                    [ 'get', '/*', 'GlobalController', [] ],
                    [ 'get', '/subdir/*', 'SubdirController', [] ],
                ],
                'get',
                '/subdir/a/b/c.def',
                [],
                'SubdirController',
                [ '*0' => [ 'a', 'b', 'c.def' ] ],
            ],
        ];
    }

    /**
     * @param array $middlewares Array of group name to array with keys of middleware class names in order.
     * @param array $routes Array of [ method, uri, handler class name, middlewares[] ]
     * @param string $inputMethod Method to be put in request.
     * @param string $inputUri URI to be put in request.
     * @param array $expectedMiddlewares Expected middlewares to be configured to pass through the response.
     * @param string $expectedHandler Expected handler to be called to generate the response.
     *                                If the handler matches the name of a middleware, then that middleware is
     *                                expected to handle the response, otherwise a new handler is.
     * @param string[] $expectedParameters Expected parameters to have been put into the request.
     * @param string|null $expectedException If not null, the class name of the exception expected to be thrown.
     * @dataProvider providerDispatch
     */
    public function testDispatch(
        array $middlewares,
        array $routes,
        string $inputMethod,
        string $inputUri,
        array $expectedMiddlewares,
        string $expectedHandler,
        array $expectedParameters = [],
        string $expectedException = null
    ) {
        if ($expectedException !== null) {
            $this->expectException($expectedException);
        }

        // Configure the router with middlewares:
        foreach ($middlewares as $groupName => $middlewareGroup) {
            $middlewareConfig = $this->router->middleware($groupName);
            foreach ($middlewareGroup as $middlewareName) {
                $middlewareConfig = $middlewareConfig->add($middlewareName);
            }
        }

        // Configure the router with routes:
        foreach ($routes as $route) {
            $routeMethod = $route[0];
            $routeUri = $route[1];
            $routeHandler = $route[2];
            $routeMiddlewares = $route[3];
            $map = $this->router->map($routeMethod, $routeUri, $routeHandler);
            foreach ($routeMiddlewares as $middleware) {
                $map = $map->middleware($middleware);
            }
        }

        // Mock initial request.
        $request = $this->createRequest($inputMethod, $inputUri);
        $nextRequest = $request;

        $gotParameters = [];
        $request->expects($this->any())
            ->method('withAttribute')
            ->will($this->returnCallback(
                function ($name, $value) use (&$gotParameters, $request) {
                    $gotParameters[$name] = $value;
                    return $request;
                }
            ));

        // Mock final response.
        $response = $this->createResponse();
        $nextResponse = $response;

        // Configure chain of middlewares.
        $errors = [];
        $handledByMiddleware = false;
        foreach ($expectedMiddlewares as $expectedMiddleware) {
            $middleware = $this->createMiddleware($expectedMiddleware);

            if ($handledByMiddleware) {
                $middleware->expects($this->never())
                    ->method('process');
                continue;
            }

            // Each middleware expects nextRequest and returns nextResponse.
            if ($expectedMiddleware === $expectedHandler) {
                $middleware->expects($this->once())
                    ->method('process')
                    ->with($this->identicalTo($nextRequest))
                    ->will($this->returnValue($nextResponse));
                $handledByMiddleware = true;
                continue;
            }

            // This request/response pair are passed to and expected from
            // the next middleware in the chain (or the handler if we're at
            // the last middleware).
            $newRequest = $this->createRequest();
            $newResponse = $this->createResponse();
            $middleware->expects($this->once())
                ->method('process')
                ->will($this->returnCallback(
                    function (
                        ServerRequestInterface $request,
                        RequestHandlerInterface $handler
                    ) use (
                        $newRequest,
                        $newResponse,
                        $nextRequest,
                        $nextResponse,
                        &$errors
                    ) {
                        $this->assertSame($request, $nextRequest);
                        $response = $handler->handle($newRequest);
                        $this->assertSame($newResponse, $response);
                        return $nextResponse;
                    }
                ));

            // Set the new request and response for the next middleware.
            $nextRequest = $newRequest;
            $nextResponse = $newResponse;
        }

        // Mock the final handler.
        if (!$handledByMiddleware) {
            $handler = $this->createHandler($expectedHandler);
            $this->containerObjects[$expectedHandler] = $handler;
            $handler->expects($this->any())
                ->method('handle')
                ->with($this->identicalTo($nextRequest))
                ->will($this->returnValue($nextResponse));
        }

        // Fire it off.
        $gotResponse = $this->router->dispatch($request);

        // Check we get the right response back.
        $this->assertSame($response, $gotResponse);

        // Check the right parameters were put in.
        ksort($expectedParameters);
        ksort($gotParameters);
        $this->assertEquals($expectedParameters, $gotParameters);
    }

    /**
     * @param string $name
     * @return MockObject|MiddlewareInterface
     */
    private function createMiddleware(string $name)
    {
        $middleware = $this->createMock(MiddlewareInterface::class);
        $this->containerObjects[$name] = $middleware;
        return $middleware;
    }

    /**
     * @param string $name
     * @return MockObject|RequestHandlerInterface
     */
    private function createHandler(string $name)
    {
        $handler = $this->createMock(RequestHandlerInterface::class);
        $this->containerObjects[$name] = $handler;
        return $handler;
    }

    /**
     * @param string $method
     * @param string $uri
     * @return MockObject|ServerRequestInterface
     */
    private function createRequest($method = '', $uri = '')
    {
        $uriObject = $this->createMock(UriInterface::class);
        $uriObject->expects($this->any())
            ->method('getPath')
            ->will($this->returnValue($uri));

        $request = $this->createMock(ServerRequestInterface::class);
        $request->expects($this->any())
            ->method('getMethod')
            ->will($this->returnValue($method));
        $request->expects($this->any())
            ->method('getUri')
            ->will($this->returnValue($uriObject));
        return $request;
    }

    /**
     * @return MockObject|ResponseInterface
     */
    private function createResponse()
    {
        return $this->createMock(ResponseInterface::class);
    }
}
