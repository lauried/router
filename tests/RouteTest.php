<?php

namespace SlightlyInteractive\Router\Test;

use PHPUnit\Framework\TestCase;
use SlightlyInteractive\Router\RouteMatch;
use SlightlyInteractive\Router\Route;

class RouteTest extends TestCase
{
    /**
     * @return array[]
     */
    public function providerMatchRequest()
    {
        return [
            // match 2 params
            [
                'get',
                '/blah/{param1}/{param2}',
                'get',
                '/blah/x/y',
                true,
                [ 'param1' => 'x', 'param2' => 'y' ],
                2,
            ],
            // match variable params
            [
                'get',
                '/blah/*',
                'get',
                '/blah/a/b/c/d/e',
                true,
                [ '*0' => [ 'a', 'b', 'c', 'd', 'e' ] ],
                5,
            ],
            [
                'get',
                '/blah/*/d/e',
                'get',
                '/blah/a/b/c/d/e',
                true,
                [ '*0' => [ 'a', 'b', 'c' ] ],
                3,
            ],
            [
                'get',
                '/blah/*/x/{y}/*/z',
                'get',
                '/blah/a/b/c/x/fish/d/e/z',
                true,
                [
                    '*0' => [ 'a', 'b', 'c' ],
                    'y' => 'fish',
                    '*1' => [ 'd', 'e' ],
                ],
                6,
            ],
            [
                'get',
                '/blah/*/s',
                'get',
                '/blah/s',
                false,
            ],
            [
                'get',
                '/blah/*',
                'get',
                '/blah',
                false,
            ],
            // don't match
            [
                'get',
                '/blah/a/b',
                'get',
                '/blah/x/y',
                false,
            ],
        ];
    }

    /**
     * @param string $routeMethod
     * @param string $routeUri
     * @param string $givenMethod
     * @param string $givenUri
     * @param bool $expectMatch
     * @param string[] $expectedParameters
     * @param int $expectedVariability
     * @dataProvider providerMatchRequest
     */
    public function testMatchRequest(
        string $routeMethod,
        string $routeUri,
        string $givenMethod,
        string $givenUri,
        bool $expectMatch,
        array $expectedParameters = [],
        int $expectedVariability = 0
    ) {
        $route = new Route($routeMethod, $routeUri, 'Nothing');
        $result = $route->matchRequest($givenMethod, explode('/', $givenUri));

        if (!$expectMatch) {
            $this->assertNull($result);
            return;
        }

        $this->assertInstanceOf(RouteMatch::class, $result);
        /** @var RouteMatch $result */
        $this->assertEquals($expectedParameters, $result->getParameters());
        $this->assertEquals($expectedVariability, $result->getVariability());
    }
}
