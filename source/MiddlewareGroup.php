<?php

namespace SlightlyInteractive\Router;

class MiddlewareGroup implements MiddlewareGroupInterface
{
    /** @var string[] */
    private $members = [];

    /**
     * @param string $middleware
     * @return MiddlewareGroupInterface
     */
    public function add(string $middleware): MiddlewareGroupInterface
    {
        $this->members[] = $middleware;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getMembers()
    {
        return $this->members;
    }
}
