<?php

namespace SlightlyInteractive\Router;

class Route implements RouteInterface
{
    /** @var MiddlewareInfo[] */
    private $middlewares = [];

    /** @var string */
    private $method;

    /** @var string */
    private $uri;

    /** @var string */
    private $handler;

    /**
     * @param $method
     * @param string $uri
     * @param string $handler
     */
    public function __construct($method, string $uri, string $handler)
    {
        $this->method = strtolower($method);
        $this->uri = $uri;
        $this->handler = $handler;
    }

    /**
     * @param string $method Lowercase version of HTTP method.
     * @param array $requestUriParts Path split into directories.
     * @return RouteMatch|null
     */
    public function matchRequest(string $method, array $requestUriParts)
    {
        $variability = 0;

        if ($this->method === '*') {
            $variability += 1;
        } elseif ($method !== $this->method) {
            return null;
        }

        $wildPart = -1;
        $matchParts = explode('/', $this->uri);
        $parameters = [];

        while (true) {
            if (empty($matchParts) !== empty($requestUriParts)) {
                return null;
            }

            if (empty($matchParts)) {
                break;
            }

            $matchPart = array_shift($matchParts);

            // The * token in the route matcher will match one or more path parts that don't match the next matcher,
            // or until the end.
            if ($matchPart === '*') {
                $wildPart += 1;
                $matchCount = 0;
                while (
                    !empty($requestUriParts)
                    && (empty($matchParts) || reset($matchParts) !== reset($requestUriParts))
                ) {
                    $parameters['*' . $wildPart][] = array_shift($requestUriParts);
                    $matchCount += 1;
                }
                if ($matchCount === 0) {
                    return null;
                }
                $variability += $matchCount;
                // If we did this, go around to get the next match parts.
                continue;
            }

            if (substr($matchPart, 0, 1) === '{' && substr($matchPart, -1) === '}') {
                $parameters[substr($matchPart, 1, -1)] = array_shift($requestUriParts);
                $variability += 1;
                continue;
            }

            if ($matchPart !== array_shift($requestUriParts)) {
                return null;
            }
        }

        return new RouteMatch($this, $parameters, $variability);
    }

    /**
     * @param string $name
     * @return RouteInterface
     */
    public function middleware(string $name): RouteInterface
    {
        $this->middlewares[] = new MiddlewareInfo($name);

        return $this;
    }

    /**
     * @param string $name
     * @return RouteInterface
     */
    public function middlewareGroup(string $name): RouteInterface
    {
        $this->middlewares[] = new MiddlewareInfo($name, MiddlewareInfo::GROUP);

        return $this;
    }

    /**
     * @return MiddlewareInfo[]
     */
    public function getMiddleware()
    {
        return $this->middlewares;
    }

    /**
     * @return string
     */
    public function getHandler()
    {
        return $this->handler;
    }
}
