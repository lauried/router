<?php

namespace SlightlyInteractive\Router;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Handler implements RequestHandlerInterface
{
    /** @var ContainerInterface */
    private $container;

    /** @var string */
    private $handler;

    /** @var string[] */
    private $handlerChain;

    /** @var Handler|null */
    private $next = null;

    /**
     * @param ContainerInterface $container
     * @param string[] $handlerChain
     */
    public function __construct(ContainerInterface $container, array $handlerChain)
    {
        $this->container = $container;
        $this->handlerChain = $handlerChain;
        $this->handler = array_shift($this->handlerChain);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $instance = $this->container->get($this->handler);

        if ($instance instanceof MiddlewareInterface) {
            if ($this->next === null) {
                $this->next = new Handler($this->container, $this->handlerChain);
            }

            return $instance->process($request, $this->next);
        }

        if ($instance instanceof RequestHandlerInterface) {
            return $instance->handle($request);
        }

        throw new \UnexpectedValueException(
            get_class($instance)
            . ' not instance of '
            . MiddlewareInterface::class
            . ' or '
            .RequestHandlerInterface::class
        );
    }
}
