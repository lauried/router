<?php

namespace SlightlyInteractive\Router;

interface RouteInterface
{
    /**
     * @param string $name
     * @return RouteInterface
     */
    public function middleware(string $name): RouteInterface;

    /**
     * @param string $name
     * @return RouteInterface
     */
    public function middlewareGroup(string $name): RouteInterface;
}
