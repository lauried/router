<?php

namespace SlightlyInteractive\Router;

interface MiddlewareGroupInterface
{
    /**
     * @param string $middleware
     * @return MiddlewareGroupInterface
     */
    public function add(string $middleware): MiddlewareGroupInterface;
}
