<?php

namespace SlightlyInteractive\Router;

class RouteMatch
{
    /** @var Route */
    private $route;

    /** @var string[] */
    private $parameters;

    /** @var int */
    private $variability;

    /**
     * @param Route $route
     * @param string[] $parameters
     * @param int $variability
     */
    public function __construct(Route $route, array $parameters, int $variability)
    {
        $this->route = $route;
        $this->parameters = $parameters;
        $this->variability = $variability;
    }

    /**
     * @return Route
     */
    public function getRoute(): Route
    {
        return $this->route;
    }

    /**
     * @return string[]
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @return int
     */
    public function getVariability(): int
    {
        return $this->variability;
    }
}