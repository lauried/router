<?php

namespace SlightlyInteractive\Router;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Router
{
    /** @var ContainerInterface */
    private $container;

    /** @var Route[] */
    private $routes = [];

    /** @var MiddlewareGroup[] */
    private $middlewareGroups = [];

    /**
     * @param ContainerInterface $container
     */
    public function setDI(ContainerInterface $container): void
    {
        $this->container = $container;
    }

    /**
     * @param string $groupName
     * @return MiddlewareGroupInterface
     */
    public function middleware($groupName = '*'): MiddlewareGroupInterface
    {
        if (!isset($this->middlewareGroups[$groupName])) {
            $this->middlewareGroups[$groupName] = new MiddlewareGroup();
        }

        return $this->middlewareGroups[$groupName];
    }

    /**
     * @param string $method
     * @param string $uri
     * @param string $handler
     * @return RouteInterface
     */
    public function map(string $method, string $uri, string $handler): RouteInterface
    {
        $route = new Route($method, $uri, $handler);
        $this->routes[] = $route;
        return $route;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws NotFoundException
     */
    public function dispatch(ServerRequestInterface $request): ResponseInterface
    {
        $match = $this->getRoute($request);

        $route = $match->getRoute();
        $middlewares = $this->getMiddlewaresForRoute($route);

        foreach ($match->getParameters() as $name => $value) {
            $request = $request->withAttribute($name, $value);
        }

        $middlewares[] = $route->getHandler();

        $handler = new Handler($this->container, $middlewares);

        return $handler->handle($request);
    }

    /**
     * @param ServerRequestInterface $request
     * @return RouteMatch
     */
    private function getRoute(ServerRequestInterface $request): RouteMatch
    {
        $method = strtolower($request->getMethod());
        $pathParts = explode('/', $request->getUri()->getPath());

        $lowestVariability = PHP_INT_MAX;
        $lowestMatch = null;

        foreach ($this->routes as $route) {
            $match = $route->matchRequest($method, $pathParts);

            if ($match === null) {
                continue;
            }

            $matchVariability = $match->getVariability();
            if ($matchVariability === 0) {
                return $match;
            }

            if ($matchVariability < $lowestVariability) {
                $lowestVariability = $matchVariability;
                $lowestMatch = $match;
            }
        }

        if ($lowestMatch === null) {
            throw new NotFoundException("No route could be found for the request");
        }

        return $lowestMatch;
    }

    /**
     * @param Route $route
     * @return string[]
     */
    private function getMiddlewaresForRoute(Route $route)
    {
        $middlewareChain = [];

        if (isset($this->middlewareGroups['*'])) {
            $middlewareChain = $this->middlewareGroups['*']->getMembers();
        }

        $routeMiddleware = $route->getMiddleware();

        foreach ($routeMiddleware as $middleware) {
            if (isset($this->middlewareGroups[$middleware->getIdentifier()])) {
                foreach ($this->middlewareGroups[$middleware->getIdentifier()]->getMembers() as $item) {
                    $middlewareChain[] = $item;
                }
            } elseif ($middleware->getType() !== MiddlewareInfo::GROUP
                && $this->container->has($middleware->getIdentifier())
            ) {
                $middlewareChain[] = $middleware->getIdentifier();
            }
        }

        return $middlewareChain;
    }
}
