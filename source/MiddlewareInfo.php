<?php

namespace SlightlyInteractive\Router;

/**
 * Class MiddlewareInfo
 *
 * Represents a route's information about the middleware it should use.
 * Gets passed to Router.
 *
 * @package SlightlyInteractive\Router
 */
class MiddlewareInfo
{
    /** @var string */
    const ANY = 'any';

    /** @var string */
    const GROUP = 'group';

    /** @var string */
    private $identifier;

    /** @var string */
    private $type;

    /**
     * @param string $identifier
     * @param string $type
     */
    public function __construct(string $identifier, string $type = self::ANY)
    {
        $this->identifier = $identifier;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}